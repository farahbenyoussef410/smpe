# SMPE
 
This GitLab project is dedicated to the homework for the "smpe" course

## 28/09/22

[✅] Read Popper’s text  
[✅] Indicate your name on the Pad  
[✅] Register on the Mattermost   
[✅] Set up a public github or gitlab project for this lecture computational documents in this project.    
[✅] Register to the MOOC on Reproducible Research  
[✅] Follow modules 1 + 2 of the MOOC with as much exercises as possible (except the last one of module2)  
[✅] Set up a computational document system (e.g., Rstudio or Jupyter on your laptop or through the UGA JupyterHub)  
[✅] Report the URL of your git project, your mattermost ID on the Pad

## 05/10/23  

[✅] Start learning R by reading this R crash course for computer scientists (Rmd sources)  
[✅] Criticize every figure of Jean-Marc’s slides by:
Applying the checklist for good graphics;  
Proposing a better representation (hand-drawing is fine) that passes the checklist  
[✅] Report this work for at least 3 figures on you github/gitlab project


## 19/10/23

[✅] Use good naming and organization conventions in your repos  
[✅] Complete the Challenger exercise  
[✅] Engage in module 3 of the MOOC and choose a topic for the peer evaluation 


## 07/12/23

[✅] Two views on Open Science. Read both documents, Write down what you learnt (what appears useful, what is surprising, …) and your opinion on potential biases in both documents


## 11/01/24 

[✅] Keep playing with the DoE Shiny Application (https://arnaud-legrand.shinyapps.io/design_of_experiments/?user_g5185)  
[✅] Pick a topic and prepare a short presentation