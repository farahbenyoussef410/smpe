# The State of Open Data 2023


Open data and research have changed quickly. Open science is important because it makes research easyer to access and helps everyone use and reuse it, which is vital for research progress. this survey received responses from a broad range of disciplines and these were its results:

the idea that early career researchers are driving data sharing forward and that more established, later career academics are opposed to progress in the space is a misconception.which was surprising to me. Researchers spanning all career lengths are meeting the same challenges and share the same motivations for data sharing. In fact they lack adequate acknowledgment for openly sharing their data. The primary motivation for sharing data among respondents was the citation of their research papers, followed by the desire to contribute to public benefit.

Roughly 75% of those surveyed have never received support with planning, managing or sharing research data. When it comes to making their research data accessible, they seek guidance particularly in the erea of copyright and data licensing.


In terms of AI utilization, when it involves employing ChatGPT or comparable AI tools for tasks like data collection, processing, or creating metadata, the predominant response indicated awareness of these tools but lack of consideration for their use.

Finaly, and what is also surprising in this survey is that 	only 43% of respondents were aware of what a data management plan (DMP).




# Passport for Open Science 



This document gave us an idea about the data management discussed earlier. It began by outlining the sources to look for data.

It then presented Research Data Management (RDM) and its contribution to the research process, encompassing all activities involved in collecting, describing, storing, processing, analyzing, archiving, and accessing data.

We initiate this process by creating a Data Management Plan (DMP), which includes:

* Documentation describing the data
* Data storage procedures
* Rules for protection
* Resources and roles of individuals involved in the project

Adopting a reproducible approach is highly beneficial due to its numerous advantages, and this document demonstrates how to implement it effectively